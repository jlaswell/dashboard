<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'before' => 'auth.sentry', function()
{
	return View::make('default');
}));

Route::get('test', function()
{
	$cho = new CyberMetrix\Cho;
	echo $cho::cho();
	return 'Hello!';
});

Route::get('mail', function()
{
	$data = array('container' => 'Hello World!');

	Mail::queue('emails.test', $data, function($message)
	{
		$message->to('jlaswell@cybermetrix.com', 'John Laswell')->subject('Test');
	});
});

/* - - - - - - - - - - - - - - - - -
 * Authentication routes
 * - - - - - - - - - - - - - - - - -
 */
Route::get('logout', array('as' => 'logout', 'uses' => 'UserController@getLogout'));
Route::get('login', array('as' => 'login', 'uses' => 'UserController@getLogin'));
Route::post('login', 'UserController@postLogin');

/* - - - - - - - - - - - - - - - - -
 * Resource routes
 * - - - - - - - - - - - - - - - - -
 */
Route::resource('users', 'UserController');
Route::resource('projects', 'ProjectsController');
