<?php

class LoginTest extends TestCase {

	public function setUp()
	{
		parent::setUp();

		Route::enableFilters();
	}
	public function testRedirectToLoginWhenNotAuthorized()
	{
		$this->client->request('GET', '/');
		$this->assertRedirectedToRoute('login');
	}
	public function testAccessToHomeWhenAuthorized()
	{
		$user = Sentry::findUserById(1);
		Sentry::login($user, false);
		
		$this->client->request('GET', '/');
		$this->assertResponseStatus(200);

		Sentry::logout();
	}

}