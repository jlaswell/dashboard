<?php

class GroupsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('groups')->truncate();

		try {
			Sentry::findGroupByName('Users')->delete();
			Sentry::createGroup(array(
				'name'		=> 'Users',
				'permissions' => array(
					'users.index'		=> 1,
					'users.view' 		=> 1,
					'users.edit' 		=> 1,
					'users.create'	=> 1,
				),
			));
		}
		catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
		{
		    echo 'Name field is required';
		}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
		{
		    echo 'Group already exists';
		}

		// Uncomment the below to run the seeder
		// DB::table('groups')->insert($groups);
	}

}
