<?php

class UsergroupsTableSeeder extends Seeder {

	public function run()
	{
		try
		{	
			$userGroup = Sentry::findGroupByName('Users');
			$user = Sentry::findUserByLogin('jlaswell@cybermetrix.com');

			$user->addGroup($userGroup);
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    echo 'Group was not found.';
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User was not found.';
		}
	}

}
