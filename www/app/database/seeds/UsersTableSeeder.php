<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('users')->truncate();
		try {
			// dd( App::environment() );
			$users = Sentry::register(array(
				'email' 		=> 'jlaswell@cybermetrix.com',
				'password' 	=> 'password',
				), true);
			// Uncomment the below to run the seeder
			// DB::table('users')->insert($users);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    echo 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    echo 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    echo 'User with this login already exists.';
		}
	}

}
