<?php

class NavigationViewCreator {

	private $navigation;

	private function initItem( $href, $class, $text ) {
		if(!isset($this->navigation)) $this->navigation = new StdClass;
		$this->navigation->$href = new StdClass;
		$this->navigation->$href->href = $href;
		$this->navigation->$href->class = $class;
		$this->navigation->$href->text = $text;
	}
	public function create($view)
	{
		if(Sentry::check()) {
			$this->navigation = new StdClass;
			if(Sentry::getUser()->hasAccess('users.index')) {
				$this->initItem(URL::route('users.index'), 'icon-torsos-all icon-2x', 'Users');
			}
		}
		$view->with('navigation', $this->navigation);
	}
}