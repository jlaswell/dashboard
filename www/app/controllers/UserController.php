<?php

class UserController extends BaseController {
	/*
   * The layout that should be used for responses.
   */
  protected $layout = 'layouts.foundation';

  public function __construct()
  {
    $this->beforeFilter('csrf', array('on' => 'post'));
    $this->beforeFilter('auth.sentry', array('except' => array('getLogout', 'getLogin', 'postLogin')));
  }

  public function getLogout()
  {
  	Sentry::logout();
  	return Redirect::route('login');

  }

  public function getLogin()
  {
  	if (Sentry::check()) {
  		return Redirect::route('home');
  	}
  	else {
  		return View::make('login');
  	}
  }

  public function postLogin()
  {
		try {
  		$credentials = array(
  			'email' 		=> Input::get('email'),
  			'password' 	=> Input::get('password'),
  		);
  		$rules = User::getAuthValidationRules();

  		$validator = Validator::make($credentials, $rules);
  		if ($validator->fails()) {
  			return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
  		}
  		// Valid input.
			$user = Sentry::authenticate($credentials, false);
			if (Sentry::check()) {
				return Redirect::to('/');
			}
			else {
				return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
			}
		}
		catch(Cartalyst\Sentry\Users\LoginRequiredException $e) {
			dd( 'error2' );
			return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
		}
		catch(Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			dd( 'error3' );
			return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
		}
		catch(Cartalyst\Sentry\Users\WrongPasswordException $e) {
			dd( 'error4' );
			return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
		}
		catch(Cartalyst\Sentry\Users\UserNotFoundException $e) {
			dd( 'error4' );
			return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
		}
		catch(Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			dd( 'error5' );
			echo 'User is not activated.';
			return Redirect::route('login')->withErrors($validator)->withInput(Input::except('password'));
		}
  }


  /**
   * Display a listing of the resource.
   */
	public function index()
	{	
		$this->layout->content = View::make('users.index');
	}

	/**
	 * Show the form for creating a new resource.
	 */
	public function create()
	{
		$this->layout->content = View::make('users.create');
	}

	/**
	 * Store a newly created resource.
	 */
	public function store()
	{
			$input = array(
				'email' 		=> Input::get('email'),
				'password'	=> Input::get('password'),
				);
			$rules = User::getCreateValidationRules();

  		$validator = Validator::make($input, $rules);
  		if ($validator->fails()) {
  			return Redirect::route('users.create')->withErrors($validator)->withInput(Input::except('password'));
  		}

  		try {
  			Sentry::register(array(
  				'email' => $input['email'],
  				'password' => $input['password']
  				),
  				true
  			);
				return Redirect::route('users.index');
  		}
  		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    dd( 'Login field is required.' );
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
			    dd( 'Password field is required.' );
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    dd( 'User with this login already exists.' );
			}
	}
	
	/**
	 * Display the specified resource.
	 */
	public function show($userId)
	{
		return $userId;
		// $this->layout->content = View::make('users.show')
			// ->with('client', Client::findOrFail($clientId))
			// ->with('user', User::findOrFail($userId));
	}

	/**
	 * Show the form for editing the specified resource.
	 */
	public function edit($clientId, $userId)
	{
		// Allow a user to edit his/her own information and allow John to edit user info as well.
		// if ((Auth::user()->id == $userId) or (Auth::user()->id == 1)) {
			// $this->layout->content = View::make('users.edit')
				// ->with('client', Client::findOrFail($clientId))
				// ->with('user', User::findOrFail($userId));
		// }
		// else {
			// return Redirect::intended('/');
		// }
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update($clientId, $userId)
	{
		// $task = Task::findOrFail($id);

		// $task->title = Input::get('title');
		// $task->description = Input::get('description');
		// $task->target_date = Input::get('target_date');
		
		// $task->save();
		
		// return Redirect::to('clients');
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy($clientId, $userId)
	{
		// echo 'destroy ' . $id;
	}
}