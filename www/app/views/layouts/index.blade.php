<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title>Dashboard</title>

  <link rel="stylesheet" href="/dashboard/packages/foundation-4.3.2/css/normalize.css" />
  <link rel="stylesheet" href="/dashboard/packages/foundation-4.3.2/css/foundation.css" />
	<!--	<link rel="stylesheet" href="/dashboard/css/author.css" /> -->
	<link rel="stylesheet" href="/dashboard/css/dev.css" />

  <script src="/dashboard/packages/foundation-4.3.2/js/vendor/custom.modernizr.js"></script>

</head>
<body>
	<br>
	<header>
		<div class="interior-header row">
			<div class="large-8 columns">
				<h1 class="subheader title">Dashboard</h1>
			</div>
			<div class="large-5 large-offset-4 columns">
				<div class="panel">
					<p>Username</p>
				</div>
			</div>
		</div>
		<div class="interior-header row">
			<div class="large-17 columns">
				<nav class="top-bar">
					<ul class="title-area">
				    <!-- Title Area -->
				    <li class="name">
							<h1><a href="#"></a></h1>
				    </li>
				    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
				    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
				  </ul>
					<section class="top-bar-section">
				    <!-- Left Nav Section -->
				    <ul class="left">
							<li class="active"><a href="#">Home</a></li>
						</ul>
					</section>
				</nav>
			</div>
		</div>
	</header>
	<br>
	<!-- Begin main page content -->
	<div class="row">
		<div class="large-3 columns">
			<div class="panel">
				<ul class="no-bullet">
					<li style="margin-bottom:0"><h5 class="subheader" style="margin-bottom:0.24em;">Notifications</h5></li>
					<hr style="margin-bottom:0.48em;margin-top:0">
					<li>Users <span class="round label right">3</span></li>
					<li>Projects</li>
					<li>Phases</li>
					<li>POs</li>
					<li>Invoices</li>
				</ul>
			</div>
		</div>
		<div class="large-14 columns">
			@yield('container')
		</div>
	</div>

  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? '/dashboard/packages/foundation-4.3.2/js/vendor/zepto' : '/dashboard/packages/foundation-4.3.2/js/vendor/jquery') +
  '.js><\/script>')
  </script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.alerts.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.clearing.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.cookie.js"></script>
  <!-- <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.dropdown.js"></script> -->
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.forms.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.joyride.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.magellan.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.orbit.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.placeholder.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.reveal.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.section.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.tooltips.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.topbar.js"></script>
  <script src="/dashboard/packages/foundation-4.3.2/js/foundation/foundation.interchange.js"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>
