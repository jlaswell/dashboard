<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title>Dashboard - @yield('title')</title>
  <link rel="stylesheet" href="{{ asset('/packages/foundation-4.3.2/css/normalize.css') }}" />
  <link rel="stylesheet" href="{{ asset('/packages/foundation-4.3.2/css/foundation.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/author.css') }}" /> 
	<link rel="stylesheet" href="{{ asset('/css/dev.css') }}" />
  <link rel="stylesheet" href="{{ asset('/fonts/dashboard/styles.css')}}" />

  <script src="{{ asset('/packages/foundation-4.3.2/js/vendor/custom.modernizr.js') }}"></script>

</head>
<body>
  @if(!empty($navigation))
    <nav class="top-bar">
      <ul class="title-area">
        <li class="name"><h1><a href="{{ URL::route('home') }}">Dashboard</a></h1></li>
        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
      </ul>
      <section class="top-bar-section">
        <ul class="left">
          <li class="divider"></li>
          <li><a href="#" class="label">0</a></li>
          <li class="divider"></li>
          <li class="has-form">
            <form>
              <div class="row collapse">
                <div class="small-13 columns">
                  <input type="text">
                </div>
                <div class="small-4 columns">
                  <a href="#" class="button secondary">Search</a>
                </div>
              </div>
            </form>
          </li>
          @foreach( $navigation as $item )
            <li><a href="{{ $item->href }}"><span>{{ $item->text }}</span><span class="show-for-small right {{ $item->class }}"></span></a></li>
          @endforeach
        </ul>
        <ul class="right">
          <li class="has-dropdown">
            <a>{{ Sentry::getUser()->email }}</a>
            <ul class="dropdown">
              <li class="has-dropdown">
                <a href="#">Settings<span class="show-for-small icon-widget icon-2x right"></span></a>
                <ul class="dropdown">
                  <li><a href="#">Profile</a></li>
                  <li><a href="#">Notifications</a></li>
                </ul>
              </li>
              <li><a href="{{ URL::route('logout') }}">Logout</a></li>
            </ul>
          </li>
        </ul>
      </section>
    </nav>
  @endif
  <br>
  @yield('content')

  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? '{{ asset("/packages/foundation-4.3.2/js/vendor/zepto") }}' : '{{ asset("/packages/foundation-4.3.2/js/vendor/jquery") }}') +
  '.js><\/script>')
  </script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.alerts.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.clearing.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.cookie.js') }}"></script>
  <!-- <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.dropdown.js') }}"></script> -->
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.forms.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.joyride.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.magellan.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.orbit.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.placeholder.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.reveal.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.section.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.tooltips.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.topbar.js') }}"></script>
  <script src="{{ asset('/packages/foundation-4.3.2/js/foundation/foundation.interchange.js') }}"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>
