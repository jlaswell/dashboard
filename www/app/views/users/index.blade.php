@section('content')
<div class="row">
	<div class="large-3 columns">
		@if(Sentry::getUser()->hasAccess('users.create'))
			{{ HTML::link( URL::route('users.create'), 'Create User', array('class' => 'button expand')) }}
		@endif
	</div>
	<div class="large-14 columns">
		<table>
			<thead>
				<tr>
					<th>email</th>
				</tr>
			</thead>
			<tbody>
				@foreach(Sentry::findAllUsers() as $user)
					<tr>
						<td>{{ $user->email }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop