@section('content')
	<div class="row">
		<div class="large-3 columns">
			<h2>New User</h2>
		</div>
		<div class="large-14 columns">
			{{ Form::open(array('route' => 'users.store')) }}
				<div class="row">
					<div class="large-6 columns">
						@if($errors->has('email'))
							<label class="error">{{ $errors->first('email') }}</label>
						@elseif(Session::has('errors.email'))
							<label class="error">{{ Session::get('errors.email') }}</label>
						@endif
						{{ Form::label('Email') }}
						{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email', 'required' => 'required', 'autocapitalize' => 'off')) }}
					</div>
				</div>
				<div class="row">
					<div class="large-6 columns">
						@if($errors->has('password'))
							<label class="error">{{ $errors->first('password') }}</label>
						@elseif(Session::has('errors.password'))
							<label class="error">{{ Session::get('errors.password') }}</label>
						@endif
						{{ Form::label('Password') }}
						{{ Form::password('password', array('placeholder' => 'Password', 'required' => 'required')) }}
					</div>
				</div>
				<div class="row">
					<div class="large-6 columns">
						{{ Form::submit('Create', array('class' => 'button')) }}
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>
@stop