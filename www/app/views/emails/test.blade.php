<!DOCTYPE html>
<html>
	<head>
		<!-- html5-boilerplate : A professional front-end template for building fast, robust, and adaptable web apps or sites. -->
		<!-- http://html5boilerplate.com/ -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title></title>
		<meta name="author" content="">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		{{ $container }}
	</body>
</html>
