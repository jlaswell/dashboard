@extends('layouts.foundation')

@section('title')
	Dashboard
@stop

@section('content')
	<div class="row">
		<div class="large-17 columns">
			<h1>Hello!</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eu urna cursus, vulputate metus vitae, sollicitudin nibh. Morbi tincidunt nibh non consectetur blandit. Sed egestas mi quis neque ornare, fringilla interdum mauris cursus. Vivamus nulla nulla, pretium et nunc a, rhoncus porttitor lorem. Maecenas sit amet blandit nisl, et rhoncus lorem. Ut a quam vehicula, commodo risus non, rhoncus risus. Nullam accumsan pharetra convallis. Integer et augue nec nulla condimentum ultricies in quis massa. Pellentesque iaculis tortor vestibulum ipsum adipiscing tincidunt. Praesent tincidunt pellentesque facilisis. Curabitur facilisis at nisl eget faucibus. Nunc imperdiet vel nulla a fermentum.</p>
			<p>Praesent venenatis a elit ut sodales. Aenean quis tristique sapien, non malesuada dui. Vivamus interdum tristique magna at elementum. Quisque varius quis mi at pulvinar. Nulla eu augue non libero tincidunt adipiscing. Sed vitae ornare libero. Integer eros purus, mattis eget leo eu, scelerisque vestibulum neque. Suspendisse eu erat eu purus accumsan vulputate sed eget ligula. Sed porttitor felis erat, rutrum adipiscing metus euismod non. Nullam dapibus eget nisl a mollis. Suspendisse potenti.</p>
		</div>
	</div>
@stop