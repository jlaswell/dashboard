@extends('layouts.foundation')

@section('title')
	Login
@stop

@section('content')
<div class="row">
	<div class="large-5 large-offset-6 columns">
		{{ Form::open() }}
			<fieldset>
				<legend>
					<h3 class="subheader">Dashboard</h3>
				</legend>
				<div class="row">
					<div class="large-17 columns">
						@if($errors->has('email'))
							<label class="error">{{ $errors->first('email') }}</label>
						@elseif(Session::has('errors.email'))
							<label class="error">{{ Session::get('errors.email') }}</label>
						@endif
						{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email', 'required' => 'required', 'autocapitalize' => 'off')) }}
					</div>
				</div>
				<div class="row">
					<div class="large-17 columns">
						@if($errors->has('password'))
							<label class="error">{{ $errors->first('password') }}</label>
						@elseif(Session::has('errors.password'))
							<label class="error">{{ Session::get('errors.password') }}</label>
						@endif
						{{ Form::password('password', array('placeholder' => 'Password', 'required' => 'required')) }}
					</div>
				</div>
				<div class="row">
					<div class="large-17 columns text-right">
						{{ Form::submit('Login', array('class' => 'button small expand' )) }}
					</div>
				</div>
			</fieldset>
		{{ Form::close() }}
	</div>
</div>
@stop
